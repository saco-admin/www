from collections import defaultdict
from pathlib import Path
from parse import parse
from os import makedirs

list_template = """
<h2>{header}</h2>
<ul class="list-group">
    {list_items}
</ul>
<br>
"""

list_item_template = """
<li class="list-group-item">
    {title}
    {hrefs}
    ({author})
    {handouts}
</li>
"""

href_template = """[<a href="{link}">{fmt}</a>]"""

class Doc:
    def __init__(self, path, relative_to):
        self.path = str(path.relative_to(relative_to))
        self.ext = path.suffix[1:].lower()
        t = path.stem.replace(')', ') ')
        self.title, self.author, etc = parse('{} ({}){}', t)
        self.handout = 'Handout' in etc
        ints = [s for s in etc.split() if s.isdigit()]
        self.version = ints[0] if ints else ''
        self.name = f'{self.title} ({self.author})'
        self.fmt = self.ext + self.version
        if 'YouTube' in etc:
            self.fmt = 'YouTube' + self.version
            with open(path) as f:
                self.path = f.readline()

def build_presentations(presentations_path):
    path = Path(presentations_path)
    makedirs(path, exist_ok=True)
    with open('tabs/presentations_items.html', 'w') as f:
        for camp in sorted(list(path.iterdir()), reverse=True):
            items = ''
            presentations = defaultdict(lambda: [])
            for file in camp.iterdir():
                doc = Doc(file, path.parent)
                presentations[doc.name].append(doc)
            for presentation_items in presentations.values():
                a = presentation_items[0]
                hrefs = ''
                handouts = ''
                for p in presentation_items:
                    if p.handout:
                        handouts += href_template.format(link=p.path, fmt=p.fmt)
                    else:
                        hrefs += href_template.format(link=p.path, fmt=p.fmt)
                if handouts:
                    handouts = ', with handout ' + handouts
                list_item = list_item_template.format(title=a.title, hrefs=hrefs, author=a.author, handouts=handouts)
                items += list_item
            f.write(list_template.format(header=camp.name, list_items=items))
