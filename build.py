#! /usr/bin/python3

import jinja2
import os
import json
from shutil import copyfile

import build_presentations

build_config = json.load(open('build_config.json'))

def main():
    build_directory = build_config['build_directory']
    presentations_directory = os.path.join(build_directory, 'presentations')

    build_presentations.build_presentations(presentations_directory)

    options = {}  # jinja variables

    html_targets = ['index.html', '502.html']
    for target in html_targets:
        s = jinja2.Environment(loader=jinja2.FileSystemLoader('./')).get_template(target).render(**options)
        path = build_directory + '/' + target
        os.makedirs(os.path.dirname(path), exist_ok=True)
        with open(path, 'w') as f:
            f.write(s)

    other_targets = ['styles.css']
    for target in other_targets:
        path = build_directory + '/' + target
        os.makedirs(os.path.dirname(path), exist_ok=True)
        copyfile(target, path)

if __name__ == '__main__':
    main()
