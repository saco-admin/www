# www

New and improved SACO Evaluator webpage.

# Setup

Copy `example.build_config.json` to `build_config.json` and adjust as necessary.

# Build

`python3 build.py`

# Run

Open `${build_directory}/index.html`

# Sublime Project (Windows)

```plain
{
    "folders":
    [
        {
            "path": "."
        }
    ],
    "build_systems":
    [
        {
            "name": "Build and open SACO HTML",
            "shell_cmd": "cd ${project_path} && build.py && start \"\" \"chrome\" \"build/index.html\""
        }
    ],
}
```
